package com.madhatter.musicpageserver.controller;

import com.madhatter.musicpageserver.model.MusicItem;
import com.madhatter.musicpageserver.repository.MusicItemRepository;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin("*")
@RequestMapping(value="/items")
public class MusicItems {
    private final MusicItemRepository musicRepo;

    public MusicItems(MusicItemRepository musicRepo){
        this.musicRepo = musicRepo;
    }

    @GetMapping()
    public List<MusicItem> getItems(){
        return (List<MusicItem>) musicRepo.findAll();
    }

    @GetMapping("/filter/{type}")
    public List<MusicItem> filterItems(@PathVariable String type){
        return musicRepo.getAllByItemType_Type(type);
    }

    @GetMapping("/itemName/{name}")
    public List<MusicItem> getItem(@PathVariable String name){
        List<MusicItem> searchTerm;
        searchTerm = musicRepo.getAllByItemType_Type(name);
        System.out.println(searchTerm);
        if (searchTerm.isEmpty()) {
            return musicRepo.getAllByNameContaining(name);
        } else {
            return searchTerm;
        }
    }

    @GetMapping("/itemId/{id}")
    public Optional<MusicItem> getItemForCart(@PathVariable long id){
        return musicRepo.findById(id);
    }

    @PostMapping("/add")
    public MusicItem addToStock(@RequestBody MusicItem musicItem){
        musicRepo.save(musicItem);
        return musicItem;
    }

    @DeleteMapping("/delete/{id}")
    public List<MusicItem> deleteItem(@PathVariable long id){
        musicRepo.deleteById(id);
        return getItems();
    }
}
