package com.madhatter.musicpageserver.model;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class CartItem {
    @Id @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long id;
    @Column
    private String image;
    @Column
    private String name;
    @Column
    private double price;
    @Column
    private int quantity;
    @ManyToOne
    @JoinColumn(name="musicItemId", referencedColumnName = "id")
    private MusicItem musicItemId;
}
