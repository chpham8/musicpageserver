package com.madhatter.musicpageserver.repository;

import com.madhatter.musicpageserver.model.ItemType;
import com.madhatter.musicpageserver.model.MusicItem;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MusicItemRepository extends CrudRepository<MusicItem, Long> {

//    @Query(nativeQuery = true, value = "SELECT * FROM music_item WHERE name LIKE '%?%';")
    List<MusicItem> getAllByNameContaining(String name);
    List<MusicItem> getAllByItemType_Type(String type);
}
