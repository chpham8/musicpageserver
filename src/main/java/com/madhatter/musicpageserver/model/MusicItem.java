package com.madhatter.musicpageserver.model;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class MusicItem {
    @Id @GeneratedValue (strategy = GenerationType.IDENTITY)
    private long id;
    @Column (length = 400)
    private String name;
    @Column (length = 2000)
    private String description;
    @Column
    private double price;
    @Column
    private String image;
    @Column
    private String quantity;
    @ManyToOne
    @JoinColumn
    private ItemType itemType;
}
