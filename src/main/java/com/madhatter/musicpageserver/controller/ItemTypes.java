package com.madhatter.musicpageserver.controller;

import com.madhatter.musicpageserver.model.ItemType;
import com.madhatter.musicpageserver.repository.ItemTypeRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@CrossOrigin
public class ItemTypes {
    private final ItemTypeRepository typeRepo;

    public ItemTypes(ItemTypeRepository typeRepo){
        this.typeRepo = typeRepo;
    }

    @GetMapping("/filter")
    public List<ItemType> getAllFilters() {
        return (List<ItemType>)typeRepo.findAll();
    }
}
