package com.madhatter.musicpageserver.controller;

import com.madhatter.musicpageserver.model.UserItem;
import com.madhatter.musicpageserver.repository.UserItemRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin
//@RequestMapping(value="/login")
public class UserItems {
    private final UserItemRepository userRepo;

    public UserItems(UserItemRepository userRepo){
        this.userRepo = userRepo;
    }

//    @GetMapping()
//    public List<UserItem> getItems(){
//        return (List<UserItem>) userRepo.findAll();
//    }

    @GetMapping("/user/{username}")
    public List<UserItem> getItem(@PathVariable String username){
        return userRepo.getByUsername(username);
    }

    @PostMapping("/add")
    public UserItem addUser(@RequestBody UserItem userItem){
        userRepo.save(userItem);
        return userItem;
    }
}
