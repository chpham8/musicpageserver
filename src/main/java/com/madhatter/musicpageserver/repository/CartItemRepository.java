package com.madhatter.musicpageserver.repository;

import com.madhatter.musicpageserver.model.CartItem;
import com.madhatter.musicpageserver.model.MusicItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CartItemRepository extends CrudRepository<CartItem, Long> {

    //    @Query(nativeQuery = true, value = "SELECT * FROM music_item WHERE name LIKE '%?%';")
    List<CartItem> getAllByNameContaining(String name);
    CartItem findByName(String name);
}
