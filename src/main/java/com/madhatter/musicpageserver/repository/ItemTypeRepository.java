package com.madhatter.musicpageserver.repository;

import com.madhatter.musicpageserver.model.ItemType;
import org.springframework.data.repository.CrudRepository;

public interface ItemTypeRepository extends CrudRepository<ItemType, Long> {
}
