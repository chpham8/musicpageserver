INSERT INTO music_db.item_type (id, type) VALUES (1, 'guitar');
INSERT INTO music_db.item_type (id, type) VALUES (2, 'bass');
INSERT INTO music_db.item_type (id, type) VALUES (3, 'keyboard');
INSERT INTO music_db.item_type (id, type) VALUES (4, 'drum');
INSERT INTO music_db.item_type (id, type) VALUES (5, 'console');
INSERT INTO music_db.item_type (id, type) VALUES (6, 'accessory');
INSERT INTO music_db.item_type (id, type) VALUES (7, 'game');