package com.madhatter.musicpageserver.controller;

import com.madhatter.musicpageserver.model.CartItem;
import com.madhatter.musicpageserver.repository.CartItemRepository;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping(value="/cart")
public class CartItems {
    private final CartItemRepository cartRepo;

    public CartItems(CartItemRepository cartRepo) {
        this.cartRepo = cartRepo;
    }

    @GetMapping()
    public List<CartItem> getItems() {
        return (List<CartItem>) cartRepo.findAll();
    }

    @PostMapping("/add")
    public CartItem addToCart(@RequestBody CartItem cartItem){
        CartItem existingItem = cartRepo.findByName(cartItem.getName());
        if (existingItem == null) {
            cartRepo.save(cartItem);
        } else {
            int quantity = existingItem.getQuantity();
            quantity += cartItem.getQuantity();
            existingItem.setQuantity(quantity);
            cartRepo.save(existingItem);
        }
        return cartItem;
    }

    @GetMapping("/empty")
    public void deleteCart(){
        cartRepo.deleteAll();
    }

    @DeleteMapping("/remove/{id}")
    public void deleteItem(@PathVariable long id){
        cartRepo.deleteById(id);
    }

    @GetMapping("/{id}")
    public Optional<CartItem> getCartById(@PathVariable long id){
        return cartRepo.findById(id);
    }

    @PutMapping("/update")
    public void updateCart(@RequestBody CartItem cartItem) {
        cartRepo.save(cartItem);
    }
}
