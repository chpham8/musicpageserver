package com.madhatter.musicpageserver.repository;

import com.madhatter.musicpageserver.model.UserItem;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserItemRepository extends CrudRepository<UserItem, Long> {
    List<UserItem> getByUsername(String username);
}

